﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_string_to_camel_case
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Kata.ToCamelCase("the-stealth-warrior"));
            Console.WriteLine(Kata.ToCamelCase("The_Stealth_Warrior"));
            Console.ReadLine();
        }
    }
}
