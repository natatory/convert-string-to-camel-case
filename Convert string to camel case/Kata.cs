﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_string_to_camel_case
{
    public class Kata
    {
        public static string ToCamelCase(string inputStr)
        {
            char[] chSeparators = { '_', '-' };
            var words = inputStr.Split(chSeparators);
            char[] charsOfWord;
            for (int i = 1; i < words.Length; i++)
            {
                charsOfWord = words[i].ToArray();
                charsOfWord[0] = words[i].ToUpper().First();
                words[i] = new string(charsOfWord);
            }
            string outStr = "";
            foreach (string word in words)
            {
                outStr += word;
            }
            return outStr;
        }
    }
}
